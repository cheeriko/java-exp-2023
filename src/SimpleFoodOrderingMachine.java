import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleFoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane receivedInfo;
    private JButton checkOutButton;
    private JLabel totalprice;
    private JButton sobaButton;
    private JButton sushiButton;
    private JButton cucumberButton;

    //private JLabel orderedItem;
    int sum=0;
    public SimpleFoodOrderingMachine() {

        tempuraButton.setIcon(new ImageIcon(this.getClass().getResource("ramen.jpg")));
        ramenButton.setIcon(new ImageIcon(this.getClass().getResource("ramen.jpg")));
        udonButton.setIcon(new ImageIcon(this.getClass().getResource("ramen.jpg")));
        sobaButton.setIcon(new ImageIcon(this.getClass().getResource("ramen.jpg")));
        sushiButton.setIcon(new ImageIcon(this.getClass().getResource("ramen.jpg")));
        cucumberButton.setIcon(new ImageIcon(this.getClass().getResource("ramen.jpg")));
        totalprice.setText("Total Price:      " + sum + "   yen");


        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",400);
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",600);
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",700);
            }
        });
        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba",800);
            }
        });
        sushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sushi",1300);
            }
        });
        cucumberButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Cucumber",300);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation=JOptionPane.showConfirmDialog(null,"Would you like to check out?",
                        "Checkout Confirmation",JOptionPane.YES_NO_OPTION);
                if(confirmation == 0){
                    JOptionPane.showMessageDialog(null,"Thank you. The total price is " + sum +  " yen.");
                    receivedInfo.setText("");
                    sum = 0;
                    totalprice.setText("Total        " + sum + " yen");
                }

            }
        });
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new SimpleFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String food, int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation==0) {
            JOptionPane.showMessageDialog(null, "Order for " + food + " received.");
            sum += price;
            receivedInfo.setText(receivedInfo.getText() + food + "　　" +price + "　 yen" +"\n");
            totalprice.setText("Total        " + sum + "  yen");
        }
    }
}